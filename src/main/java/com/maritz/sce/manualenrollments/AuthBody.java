package com.maritz.sce.manualenrollments;

public class AuthBody {
    
    private String clientID;
    private String clientSecret;
    
    public String getClientID() {
        return clientID;
    }
    
    public void setClientID(String clientID) {
        this.clientID = clientID;
    }
    
    public String getClientSecret() {
        return clientSecret;
    }
    
    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

}
