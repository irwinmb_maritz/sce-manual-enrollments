package com.maritz.sce.manualenrollments;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class ManualEnrollmentsApplication {
    
    private static final Logger log = LoggerFactory.getLogger(ManualEnrollmentsApplication.class);
    
    private static final String GRANT_TYPE = "client_credentials";
    
    @Value("${sce.api.client.id}")
    private String clientId;
    
    @Value("${sce.api.client.secret}")
    private String clientSecret;
    
    @Value("${sce.host}")
    private String host;
    
    @Value("${sce.triggerDownload.bypass}")
    private boolean triggerDownloadBypass;
    
    private String authEndpoint = "/oauth2/tokens?grant_type={grantType}";
    private String postmanEchoEndpoint = "https://postman-echo.com/post";
    private String triggerDownloadEndpoint = "/batchjob/vendor";
    private String downloadExtractEndpoint = "/batch/{batchId}/files?fileIdentifier=VendorSetup_{batchId}.csv&flag=false";
    
    private static final String TEST_BATCH_ID = "107";

	public static void main(String[] args) {
		SpringApplication.run(ManualEnrollmentsApplication.class, args).close();
	}
	
	@Bean
	public CommandLineRunner run() throws Exception {
	    
	    return args -> {
	        log.info("HOST ===========> " + host);
	        AuthToken authToken = getAuthToken();
	        log.info(authToken.toString());
	        
	        HttpEntity<String> request = buildAuthenticatedRequest(authToken);

	        if (!triggerDownloadBypass) {
    	        // Vendor Extract
    	        ResponseEntity<String> extractResponse = triggerExtract(request);
    	        log.info(extractResponse.getBody());
    	        
    	        if (isSuccessfulCall(extractResponse.getStatusCode()) && hasRecords(extractResponse.getBody())) {
    	           // Vendor Extract Download
                    ResponseEntity<byte[]> downloadResponse = downloadExtract(request, TEST_BATCH_ID);
                    Files.write(Paths.get("VendorSetup_" + TEST_BATCH_ID + ".csv"), downloadResponse.getBody());
                    log.info(downloadResponse.getBody().toString());
    	        }
	        } else {
    	        // Vendor Extract Download
    	        ResponseEntity<byte[]> downloadResponse = downloadExtract(request, TEST_BATCH_ID);
    	        Files.write(Paths.get("VendorSetup_" + TEST_BATCH_ID + ".csv"), downloadResponse.getBody());
    	        log.info(downloadResponse.getBody().toString());
	        }
	    };
	}
	
	private AuthToken getAuthToken() {
        RestTemplate restTemplate = new RestTemplateBuilder().build();
        
        HttpHeaders authHeaders = new HttpHeaders();
        authHeaders.setContentType(MediaType.APPLICATION_JSON);
        
        JSONObject authBody = new JSONObject();
        authBody.put("clientID", clientId);
        authBody.put("clientSecret", clientSecret);
        
        HttpEntity<String> authRequest = new HttpEntity<String>(authBody.toString(), authHeaders);
        
        ResponseEntity<AuthToken> respAuthToken = restTemplate.postForEntity(host + authEndpoint, authRequest, AuthToken.class, GRANT_TYPE);
        return respAuthToken.getBody();
	    
	}
	
	private HttpEntity<String> buildAuthenticatedRequest(AuthToken authToken) {
        HttpHeaders sceHeaders = new HttpHeaders();
        sceHeaders.set("Authorization", "Bearer " + authToken.getAccessToken());        
        return new HttpEntity<String>(null, sceHeaders);
	}
	
	private ResponseEntity<String> triggerExtract(HttpEntity<String>  request) {
        RestTemplate restTemplate = new RestTemplateBuilder().build();
        ResponseEntity<String> response = restTemplate.postForEntity(postmanEchoEndpoint, request, String.class);
        return response;
	    
	}

    private ResponseEntity<byte[]> downloadExtract(HttpEntity<String>  request, String batchId) {
        Map<String, String> params = new HashMap<>();
        params.put("batchId", batchId);
        RestTemplate restTemplate = new RestTemplateBuilder().build();
        ResponseEntity<byte[]> response = restTemplate.exchange(host + downloadExtractEndpoint, HttpMethod.GET, request, byte[].class, params);
        return response;
        
    }
    
    private boolean isSuccessfulCall(HttpStatus status) {
        return HttpStatus.OK.equals(status);
    }
    
    private boolean hasRecords(String responseBody) {
        return !responseBody.contains("No records were found");
    }
    
    private String parseBatchId(String responseBody) {
        return TEST_BATCH_ID;
    }
}
